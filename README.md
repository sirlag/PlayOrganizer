# PlayOrganizer
By default, when downloading albums from google play, google places them all into your download directory, unsorted.
This is really annoying. However, by using the id3 tags supplied with the download, you can quickly and automatically
sort them. 

PlayOrganizer is a tiny kotlin based JavaFX application, designed to do just that. It watches your Google Play Music
download folder, and when it sees a new file was created, it moves it to your music library in a folder titled {Artist} - {Album}({Year})

## Usage

To run PlayOrganizer, simply start the jar, either through your file manager or through your terminal.
 
     java -jar PlayOrganizer.jar
 
You can also run it in headless mode from your terminal of choice, by passing it the --nogui (or -n) flag, and following it with your -s (source) and -t (target) folders

    java -jar PlayOrganizer.jar -n -s {path/to/downloads/folder} -t {path/to/music/library}


## Building

PlayOrganizer is built using Gradle, so most of the dependencies you need are pulled directly from the build script.
However, one dependency that PlayOrganizer depends on, kotlin-cli, is not currently hosted in a maven repository, or
distributed as a .jar. In order to successfully build it, you will have to acquire a copy of kotlin-cli from the owner's
repo located [here](https://github.com/leprosus/kotlin-cli), and either attach it as a jar, or place the contents of the
src folder into the src/kotlin folder of PlayOrganizer before building.


Once you have fulfilled that dependency, all you need to do is run.

To build the project : 

    $ ./gradlew build


To package it as an executable :

    $ ./gradlew jar

## Tooling

PlayOrganizer was a learning project, as well as useful tool for me. The tools used to make it were :

* Kotlin - The JVM Language in development by JetBrains.
* Kovanant - A Kotlin Library for promises.
* RxKotlin - The Kotlin RxJava wrapper
* JAudioTagger - A great Library for JVM languages that allows access to id3 tags on many types of audio files.
* TornadoFX - A Kotlin wrapper for JavaFX, with a much nicer syntax using type-safe builders
* JFoeniX - A MDL style library for JavaFX, giving us our great looking buttons and text fields.

## License

PlayOrganizer is licensed under the MIT license. For more information, visit the `LICENSE` file.