package com.gitlab.sirlag

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXTextField
import javafx.geometry.Insets
import javafx.scene.control.ContextMenu
import javafx.scene.control.TableView
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.scene.paint.Color
import javafx.stage.DirectoryChooser
import javafx.stage.WindowEvent
import nl.komponents.kovenant.Kovenant
import nl.komponents.kovenant.task
import tornadofx.*
import java.awt.Desktop
import java.io.File

class PlayOrganizerApp : App(){
    override val primaryView = PlayOrganizerView::class
}

class PlayOrganizerView : View() {
    override val root = BorderPane()

    var sourceBox: TextField by singleAssign()
    var targetBox: TextField by singleAssign()

    init {
        primaryStage.setOnCloseRequest{ it: WindowEvent? ->
            folderSubscription?.unsubscribe()
            PathObservables.shouldWatch = false
            System.exit(0)
            Kovenant.stop()
        }
        with(root) {
            center = tableview(observableListOfSongs) {
                column("Song", Song::title)
                column("Artist", Song::artist)
                column("Album", Song::album)
                column("Year", Song::year)
                columnResizePolicy = TableView.CONSTRAINED_RESIZE_POLICY

                contextMenu = ContextMenu().apply {
                    menuitem("Open In File Explorer"){
                        selectedItem?.apply {
                            Desktop.getDesktop().open(File(targetFolder))
                        }
                    }
                    menuitem("Play Song"){
                        selectedItem?.apply {
                            Desktop.getDesktop().open(File(targetFolder+file))
                        }
                    }
                }
            }
            bottom = hbox {
                padding = Insets(5.0)
                spacing = 7.5
                vbox {
                    sourceBox = JFXTextField().apply {
                        isEditable = false
                    }
                    this += sourceBox
                    this += JFXButton("Select source directory").apply {
                        style {
                            padding = box(0.7.em, .57.em)
                            fontSize = 14.px
                            buttonType = JFXButton.ButtonType.RAISED
                            backgroundColor = multi(c(77, 102, 204))
                            prefWidth = 250.px
                            textFill = Color.WHITE
                        }
                        setOnAction {
                            val directoryChooser = DirectoryChooser()
                            directoryChooser.title = "Choose the folder to watch"
                            var tPath = sourceBox.text
                            try {
                                tPath = directoryChooser.showDialog(primaryStage).canonicalPath
                            } catch (ignored: Exception) {}
                            sourceBox.text = tPath
                            directory.source = tPath
                        }
                    }
                }
                vbox {
                    targetBox = JFXTextField().apply{
                        isEditable = false
                    }
                    this += targetBox
                    this += JFXButton("Select target directory").apply {
                        setOnAction {
                            val directoryChooser = DirectoryChooser()
                            directoryChooser.title = "Choose the folder to place your songs"
                            var tPath = targetBox.text
                            try {
                                tPath = directoryChooser.showDialog(primaryStage).canonicalPath
                            } catch (ignored: Exception) {}
                            targetBox.text = tPath
                            directory.target = tPath
                        }
                        style {
                            padding = box(0.7.em, .57.em)
                            fontSize = 14.px
                            buttonType = JFXButton.ButtonType.RAISED
                            backgroundColor = multi(c(77, 102, 204))
                            prefWidth = 250.px
                            textFill = Color.WHITE
                        }
                    }
                }
                vbox{
                    this += JFXButton("Start").apply {
                        setOnAction {
                            watchFolder()
                            isDisable = true
                            text = "running"
                        }
                        style {
                            padding = box(0.7.em, .57.em)
                            fontSize = 14.px
                            buttonType = JFXButton.ButtonType.RAISED
                            backgroundColor = multi(c(77, 102, 204))
                            prefWidth = 100.px
                            textFill = Color.WHITE
                        }
                    }
                }
            }
        }
    }
}