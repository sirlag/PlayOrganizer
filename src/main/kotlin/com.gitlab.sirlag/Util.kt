package com.gitlab.sirlag

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

fun File.moveToDirectory(targetDirectory: Path){
    if (!Files.exists(targetDirectory)){
        Files.createDirectories(targetDirectory)
    }
    Files.move(this.toPath(), Paths.get("$targetDirectory/${this.name}"))
}

data class Song(val artist: String, val album: String, val year: String, val file: String, val title: String){
    val targetFolder = "${directory.target}/$artist - $album($year)/"
}

data class DirectoryPair(var source: String, var target: String)