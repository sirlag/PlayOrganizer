package com.gitlab.sirlag

import com.evalab.core.cli.Command
import com.evalab.core.cli.exception.OptionException
import javafx.application.Application
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import nl.komponents.kovenant.task

import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey

import rx.Subscription

import java.io.File
import java.nio.file.Paths
import java.nio.file.StandardWatchEventKinds
import java.util.*

var folderSubscription : Subscription? = null
var directory = DirectoryPair("","")

fun main(args: Array<String>) {
    val launcherOptions = Command("launcher options", "Allows you to toggle between gui and no GUI mode")
    launcherOptions.addBooleanOption("noGui", false, 'n', "If this flag is present, PlayOrganizer starts in no GUI mode.")

    val directories = Command("java -jar PlayOrganizer.jar -n", "Sets the source and target directory for PlayOrganizer")
    directories.addStringOption("source", true, 's', "The Directory for PlayOrganizer to watch for downloads")
    directories.addStringOption("target", true, 't', "The Directory for PlayOrganizer move the downloaded music into")

    try {
        launcherOptions.parse(args)
        if (launcherOptions.getBooleanValue("noGui", false)!!) {
            try {
                directories.parse(args)
                directory.source = directories.getStringValue("source")!!
                directory.target = directories.getStringValue("target")!!
                watchFolder(true)
            } catch (e: OptionException) {
                println(e.message)
                println(directories.getHelp())
            }
        } else {
            Application.launch(PlayOrganizerApp::class.java, *args)
        }
    } catch (e: OptionException) {
        println(e.message)
        println(launcherOptions.getHelp())
        System.exit(2)
    }
}

fun watchFolder(noGui: Boolean = false): Boolean{

    if (folderSubscription != null)
        folderSubscription?.unsubscribe()

    val watchedPath =
        PathObservables.watchFolder(Paths.get(directory.source))
        .filter { it.kind() == StandardWatchEventKinds.ENTRY_CREATE }
        .map { getSongInformation(directory.source, it.context().toString()) }
    folderSubscription = watchedPath
        .subscribe {
            task {
                File("${directory.source}/${it.file}").moveToDirectory(Paths.get("${it.targetFolder}"))
                if (noGui)
                    println("Moved Song: ${it.file}")
                else
                    Platform.runLater { observableListOfSongs.add(it) }
            }
        }
    return true

}

val observableListOfSongs: ObservableList<Song> = FXCollections.observableArrayList(ArrayList<Song>())

fun getSongInformation(path:String, file:String): Song {
    val song = AudioFileIO.read(File( "$path/$file"))
    val tags = song.tag
    return Song(tags.getFirst(FieldKey.ARTIST), tags.getFirst(FieldKey.ALBUM), tags.getFirst(FieldKey.YEAR), file, tags.getFirst(FieldKey.TITLE))
}
