package com.gitlab.sirlag

import rx.Observable
import nl.komponents.kovenant.task
import java.io.IOException
import java.nio.file.*


object PathObservables {
    var shouldWatch = false

    fun watchFolder(directory: Path): Observable<WatchEvent<*>>{
        return Watcher(directory).create()
    }

    private class Watcher @Throws (IOException::class)
    constructor(private val folderToWatch: Path){
        private val watchService: WatchService

        init {
            val fileSystem = folderToWatch.fileSystem
            watchService = fileSystem.newWatchService()
            shouldWatch = true
        }

        fun create(): Observable<WatchEvent<*>>{
            return Observable.create { subscriber ->
                var errorFree = true
                try{
                    folderToWatch.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY)
                } catch (exception: IOException){
                    subscriber.onError(exception)
                    errorFree = false
                }
                task {
                    while (errorFree && shouldWatch) {
                        val key: WatchKey
                        try {
                            key = watchService.take()
                        } catch (exception: InterruptedException) {
                            subscriber.onError(exception)
                            errorFree = false
                            break
                        }

                        for (event in key.pollEvents()) {
                            subscriber.onNext(event)
                        }

                        val valid = key.reset()
                        if (!valid) {
                            break
                        }
                    }
                } success {
                    subscriber.onCompleted()
                }
            }
        }
    }
}